## Codestyle
1. Поместить в проект файлы *.eslintrc.yml* и *.stylelintrc.yml*
2. Установить необходимые пакеты, например:
   ```
    "eslint": "6.8.0",
    "eslint-config-airbnb": "18.0.1",
    "eslint-plugin-import": "2.20.1",
    "eslint-plugin-jsx-a11y": "6.2.3",
    "eslint-plugin-react": "7.18.3",
    "eslint-plugin-react-hooks": "2.4.0",
    "stylelint": "13.2.0"
    ```
3. Для сборки вебпаком добавляем соотвествующие лоадеры и плагины, например:
   ```
   "eslint-loader": "3.0.3",
   "stylelint-webpack-plugin": "1.2.3",
   "babel-eslint": "10.0.3",
   ```


### Автоформатирование для vscode
- поставить плагины *eslint* и *stylelint*
- Добавить в файл настроек команду на сохранение

*settings.json*
```
"editor.codeActionsOnSave": {
    "source.fixAll": true
  },
```

[Инструкция для других редакторов](https://medium.com/@netczuk/even-faster-code-formatting-using-eslint-22b80d061461)